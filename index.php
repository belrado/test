<?php
require_once __DIR__."/vendor/autoload.php";

use App\Test;

//phpinfo();

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$app = new Test($path);
$app->process();
?>